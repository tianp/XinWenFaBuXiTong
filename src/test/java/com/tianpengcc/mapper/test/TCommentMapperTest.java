package com.tianpengcc.mapper.test;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tianpengcc.service.LinkService;



public class TCommentMapperTest {
	private BeanFactory beanFacotry ;
	
	@Before
	public void setUp() throws Exception {
		beanFacotry = new ClassPathXmlApplicationContext("classpath:spring-config/applicationContext-dao.xml","classpath:spring-config/applicationContext-service.xml","classpath:spring-config/applicationContext-transaction.xml") ;
	}

	@Test
	public void testSelectByPrimaryKey() throws Exception {    
		LinkService linkService =  beanFacotry.getBean("linkService",LinkService.class);
		System.out.println(linkService.findAllByOrderNumASC());
	}

}
