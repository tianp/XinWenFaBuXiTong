package com.tianpengcc.pojo;

/**
 * 
 * <p>Title:Comment</p>
 * <p>Description:评论扩展类</p>
 * <p>Compary</p>
 * @author 田鹏
 * @date 2017年11月2日下午3:13:52
 */
public class Comment extends TComment{
	private TNews news ;

	public TNews getNews() {
		return news;
	}

	public void setNews(TNews news) {
		this.news = news;
	}
}
