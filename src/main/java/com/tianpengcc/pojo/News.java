package com.tianpengcc.pojo;

/**
 * 
 * <p>Title:News</p>
 * <p>Description:新闻的扩展类</p>
 * <p>Compary</p>
 * @author 田鹏
 * @date 2017年10月30日下午9:29:30
 */
public class News extends TNews{
	private TNewstype tNewstype ;

	public TNewstype gettNewstype() {
		return tNewstype;
	}

	public void settNewstype(TNewstype tNewstype) {
		this.tNewstype = tNewstype;
	}
	
}
