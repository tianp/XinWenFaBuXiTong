package com.tianpengcc.service;

import java.util.List;

import com.tianpengcc.pojo.TLink;

/**
 * 
 * <p>Title:LinkService</p>
 * <p>Description:友情链接的Service</p>
 * <p>Compary</p>
 * @author 田鹏
 * @date 2017年10月30日下午3:46:53
 */
public interface LinkService {
	
	/**
	 * 
	 * <p>Description:根据orderNum升序查询出所有的友情链接</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年10月30日下午3:49:39
	 */
	public List<TLink> findAllByOrderNumASC() throws Exception ;
	
	/**
	 * 
	 * <p>Description:根据友情链接的Id，取得友情链接对象</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月1日下午8:45:00
	 */
	public TLink findByLinkId(int linkId) throws Exception ;
	
	/**
	 * 
	 * <p>Description:添加一个友情链接</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月1日下午3:03:45
	 */
	public void doCreate(String linkName,String linkUrl,String email,Integer orderNum) throws Exception ;
	
	/**
	 * 
	 * <p>Description:更新操作</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月1日下午8:31:44
	 */
	public void doUpdate(int linkId,String linkName,String linkUrl,String email,String orderNum) throws Exception ;

	/**
	 * 
	 * <p>Description:删除操作</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月1日下午8:32:45
	 */
	public void doDelete(int linkId) throws Exception ;
}
