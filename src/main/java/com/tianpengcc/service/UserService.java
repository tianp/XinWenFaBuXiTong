package com.tianpengcc.service;

import com.tianpengcc.pojo.TUser;

/**
 * 
 * <p>Title:UserService</p>
 * <p>Description:后台管理员用户的Service</p>
 * <p>Compary</p>
 * @author 田鹏
 * @date 2017年10月31日下午11:30:54
 */
public interface UserService {
	
	/**
	 * 
	 * <p>Description:登录操作</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年10月31日下午11:31:36
	 */
	public TUser login(String username,String password) throws Exception ;
}
