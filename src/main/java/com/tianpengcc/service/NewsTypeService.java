package com.tianpengcc.service;

import java.util.List;

import com.tianpengcc.pojo.NewsType;
import com.tianpengcc.pojo.TNewstype;

/**
 * 
 * <p>Title:NewsTypeService</p>
 * <p>Description:新闻类别的service</p>
 * <p>Compary</p>
 * @author 田鹏
 * @date 2017年10月30日下午8:11:09
 */
public interface NewsTypeService {
	
	/**
	 * 
	 * <p>Description:根据新闻类型的id进行查找</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月1日下午10:22:46
	 */
	public TNewstype findByNewsTypeId(Integer newsTypeId) throws Exception ;
	
	/**
	 * 
	 * <p>Description:查询所有的新闻类别，并级联新闻</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年10月30日下午8:11:25
	 */
	public List<NewsType> findAll() throws Exception ;
	
	/**
	 * 
	 * <p>Description:取得所有的新闻类别，没有级联新闻</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月1日下午9:50:06
	 */
	public List<TNewstype> findAllNotNews() throws Exception ;
	
	/**
	 * 
	 * <p>Description:添加一个新闻类别</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月1日下午9:59:43
	 */
	public void doCreate(String newsTypeName) throws Exception ;
	
	/**
	 * 
	 * <p>Description:更新操作</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月1日下午10:03:50
	 */
	public void doUpdate(Integer newsTypeId,String newsTypeName) throws Exception ;
	
	/**
	 * 
	 * <p>Description:删除操作</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月1日下午10:05:53
	 */
	public boolean doDelete(Integer newsTypeId) throws Exception ;
}
