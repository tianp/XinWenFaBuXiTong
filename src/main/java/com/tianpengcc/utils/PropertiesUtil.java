package com.tianpengcc.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 
 * <p>Title:PropertiesUtil</p>
 * <p>Description:properties工具类</p>
 * <p>Compary</p>
 * @author 田鹏
 * @date 2017年10月24日下午5:11:03
 */
public class PropertiesUtil {
	public static String getValue(String key) throws IOException{
		Properties properties = new Properties();
		InputStream inputStream = PropertiesUtil.class.getResourceAsStream("/news.properties");
		properties.load(inputStream);
		String value = properties.getProperty(key);
		return value ;
	}
	
	public static void main(String[] args) throws IOException {
		System.out.println(getValue("dbUrl"));
	}
}
