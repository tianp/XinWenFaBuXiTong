package com.tianpengcc.utils;

/**
 * 
 * <p>Title:JsonFlag</p>
 * <p>Description:json的标志位</p>
 * <p>Compary</p>
 * @author 田鹏
 * @date 2017年11月1日下午9:20:58
 */
public class JsonFlag {

	private boolean flag ;

	public boolean getFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
}
