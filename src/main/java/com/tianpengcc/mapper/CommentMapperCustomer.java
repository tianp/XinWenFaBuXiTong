package com.tianpengcc.mapper;

import java.util.List;

import com.tianpengcc.pojo.Comment;
import com.tianpengcc.pojo.TComment;
import com.tianpengcc.vo.NewsFindByTypeIdVo;

/**
 * 
 * <p>Title:CommentMapperCustomer</p>
 * <p>Description:评论的Mapper</p>
 * <p>Compary</p>
 * @author 田鹏
 * @date 2017年10月31日下午9:56:51
 */
public interface CommentMapperCustomer {
	
	/**
	 * 
	 * <p>Description:根据新闻的ID查询评论</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年10月31日下午9:57:02
	 */
	public List<TComment> findByNewsId(int newsId) throws Exception ;
	
	/**
	 * 
	 * <p>Description:添加操作</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年10月31日下午10:12:58
	 */
	public void doCreate(TComment comment) throws Exception ;
	
	/**
	 * 
	 * <p>Description:分页查询，如果有日期则根据日期查询</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月2日下午2:34:15
	 */
	public List<TComment> findByPageAndDate(NewsFindByTypeIdVo<TComment> vo) throws Exception ;

	/**
	 * 
	 * <p>Description:查询所有评论的个数</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月2日下午2:50:10
	 */
	public int findCount() throws Exception ;
	
	/**
	 * 
	 * <p>Description:分页查询，如果有时间条件则加上时间条件，如果没有就不加，返回级联的评论对象，级联新闻</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月2日下午3:15:48
	 */
	public List<Comment> findCommentByPageAndDate(NewsFindByTypeIdVo<Comment> vo) throws Exception ;
	
	/**
	 * 
	 * <p>Description:单删除，根据评论的id来删除</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月2日下午3:53:21
	 */
	public void doDelete(int commentId) throws Exception ;
	
	/**
	 * 
	 * <p>Description:多删除，根据评论的id来删除</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月2日下午3:54:05
	 */
	public void doDeleteByPiliang(List<Integer> commentIds) throws Exception ;
}
