package com.tianpengcc.mapper;

import java.util.List;

import com.tianpengcc.pojo.NewsType;
import com.tianpengcc.pojo.TNewstype;

/**
 * 
 * <p>Title:NewsTypeMapperCustomer</p>
 * <p>Description:新闻类型的Mapper扩展</p>
 * <p>Compary</p>
 * @author 田鹏
 * @date 2017年10月30日下午9:33:43
 */
public interface NewsTypeMapperCustomer {
	/**
	 * 
	 * <p>Description:查询全部的新闻类型和新闻类型下的新闻，谨慎使用</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年10月30日下午9:33:59
	 */
	public List<NewsType> findAllAndNews() ;
	
	/**
	 * 
	 * <p>Description:查询所有的新闻类别，级联为空</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年10月31日上午12:04:40
	 */
	public List<NewsType> findAll() ;
	
}
