package com.tianpengcc.mapper;

import com.tianpengcc.pojo.TUser;

/**
 * 
 * <p>Title:UserMapperCustomer</p>
 * <p>Description:后台管理员用户的Mapper</p>
 * <p>Compary</p>
 * @author 田鹏
 * @date 2017年10月31日下午11:27:25
 */
public interface UserMapperCustomer {
	
	/**
	 * 
	 * <p>Description:根据用户查询用户</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年10月31日下午11:27:43
	 */
	public TUser findByUser(TUser user) throws Exception ;
}
