package com.tianpengcc.mapper;

import java.util.List;

import com.tianpengcc.pojo.TLink;

/**
 * 
 * <p>Title:LinkMapperCustomer</p>
 * <p>Description:</p>
 * <p>Compary</p>
 * @author 田鹏
 * @date 2017年11月1日下午2:56:28
 */
public interface LinkMapperCustomer {
	
	/**
	 * 
	 * <p>Description:查询所有的友情链接，排序为升序</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月1日下午2:58:51
	 */
	public List<TLink> findAllByOrderNumASC() throws Exception ;
	
	/**
	 * 
	 * <p>Description:根据友情链接的Id进行查找</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月1日下午8:43:19
	 */
	public TLink findByLinkId(int linkId) throws Exception ;
	
	/**
	 * 
	 * <p>Description:添加一个友情链接到数据库</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月1日下午2:59:14
	 */
	public void doCreate(TLink link) throws Exception ;
	
	/**
	 * 
	 * <p>Description:更新一个友情链接</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月1日下午8:24:29
	 */
	public void doUpdate(TLink link) throws Exception ;
	
	/**
	 * 
	 * <p>Description:根据友情链接的Id来删除一个友情链接，谨慎使用，使用的时候先取，使用的时候先取，使用的时候先取</p>
	 * <p>Compary</p>
	 * @author 田鹏
	 * @date 2017年11月1日下午8:25:43
	 */
	public void doDelete(int linkId) throws Exception ;
}
